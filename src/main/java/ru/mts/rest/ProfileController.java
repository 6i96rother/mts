package ru.mts.rest;

import ru.mts.pojo.User;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/profile")
public class ProfileController {

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public User getProfile() {

        return new User();

    }

}